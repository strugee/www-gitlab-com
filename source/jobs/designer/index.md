---
layout: markdown_page
title: "Designer Responsibilities and Tasks"
---

## Responsibilities

* Set a new standard for style. 
* Work on the house style of GitLab.
* Update the looks of GitLab the application, the brand, our website, apps, and marketing materials.
* Spread the gospel of good design

## Tasks
* Create anything from quick mockups to pixel-perfect work
* Create exciting marketing materials